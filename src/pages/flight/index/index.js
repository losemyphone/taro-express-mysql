import { adsReq } from '@/common/api'
import Tab from '@/components/Tab'
import { Button, Image, Swiper, SwiperItem, Text, View } from '@tarojs/components'
import Taro from '@tarojs/taro'
import dayjs from 'dayjs'
import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import './index.scss'

const FLIGHT_TABS = [
  {
    label: '单程',
    id: 0
  },
  {
    label: '多程',
    id: 1
  },
  {
    label: '往返',
    id: 2
  }
]

// 将Redux store中的state映射到组件的props中
const mapStateToProps = ({ flightIndex }) => {
  return flightIndex
}

function FlightIndex(props) {
  console.log('🚀 ~ FlightIndex ~ props:', props)
  const [adList, setAdList] = useState([])
  const { arrCityName, dptCityName, dptDate } = props

  useEffect(() => {
    getAds()
  }, [])

  const getAds = async () => {
    const { result } = await adsReq()
    setAdList(result || [])
  }

  const handleTabClick = id => {
    console.log(id)
  }

  const chooseFlightCity = type => {
    props.dispatch({
      type: 'flightIndex/updateState',
      payload: {
        cityType: type
      }
    })
    // 注意：使用app.config.js中注册的绝对路径，需要加/
    Taro.navigateTo({
      url: '/pages/airportList/airportList'
    })
  }

  const chooseFlightDate = () => {
    Taro.navigateTo({
      url: '/pages/calendar/calendar'
    })
  }

  return (
    <View className="flight-container">
      <View className="flight-top">
        <Tab tabList={FLIGHT_TABS} onTabClick={handleTabClick} className="flight-index-tab">
          <SwiperItem>
            <View className="item station">
              <View className="cell from" onClick={() => chooseFlightCity('depart')}>
                {dptCityName}
              </View>
              <Text className={`icon-zhihuan iconfont `}></Text>
              <View className={`cell to `} onClick={() => chooseFlightCity('arrive')}>
                {arrCityName}
              </View>
            </View>
            <View className="item date" onClick={chooseFlightDate}>
              {dayjs(dptDate).format('M月D日')}
            </View>
            <Button className="search-btn">机票查询</Button>
          </SwiperItem>
          <SwiperItem>222</SwiperItem>
          <SwiperItem>333</SwiperItem>
        </Tab>
      </View>
      <View className="alipay-swiper" style={{ margin: '15px' }}>
        <Swiper className="advs-banner-bd" autoplay circular interval={3000}>
          {adList.map(item => {
            return (
              <SwiperItem key={item.id} className="item">
                <Image className="img" src={item.imgUrl}></Image>
              </SwiperItem>
            )
          })}
        </Swiper>
      </View>
    </View>
  )
}

export default connect(mapStateToProps)(FlightIndex)
