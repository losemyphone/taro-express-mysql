import { Text, View } from '@tarojs/components'
import Taro from '@tarojs/taro'
import { connect } from 'react-redux'
import './index.scss'

// 将Redux store中的state映射到组件的props中
const mapStateToProps = ({ flightIndex }) => {
  return flightIndex
}

function CityItem(props) {
  const { cityList, label } = props

  const onCityClick = cityInfo => {
    const { cityType } = props
    const { cityId, cityName, airportName } = cityInfo
    props.dispatch({
      type: 'flightIndex/updateState',
      payload:
        cityType === 'depart'
          ? { dptCityId: cityId, dptAirportName: airportName, dptCityName: cityName }
          : { arrCityId: cityId, arrAirportName: airportName, arrCityName: cityName }
    })
    Taro.navigateBack()
  }

  return (
    <View className="list-item" id={label}>
      <Text className="label">{label}</Text>
      {cityList?.map(item => {
        return (
          <View key={item.id} className="name" onClick={() => onCityClick(item)}>
            {`${item.cityName}（${item.airportName}）`}
          </View>
        )
      })}
    </View>
  )
}

export default connect(mapStateToProps)(CityItem)
// export default CityItem
