import { MAX_DATE, MIN_DATE } from '@/common/constant'
import { View } from '@tarojs/components'
import Taro from '@tarojs/taro'
import { connect } from 'react-redux'
import { AtCalendar } from 'taro-ui'

// 将Redux store中的state映射到组件的props中
const mapStateToProps = ({ flightIndex }) => {
  return flightIndex
}

function Calendar(props) {
  const { dptDate } = props

  const onDateSelect = date => {
    const {
      value: { start }
    } = date

    props.dispatch({
      type: 'flightIndex/updateState',
      payload: {
        dptDate: start
      }
    })
    Taro.navigateBack()
  }

  return (
    <View className="calendar-page">
      <AtCalendar currentDate={dptDate} minDate={MIN_DATE} maxDate={MAX_DATE} onSelectDate={onDateSelect}></AtCalendar>
    </View>
  )
}

export default connect(mapStateToProps)(Calendar)
// export default Calendar
