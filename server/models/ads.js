const express = require('express')
const router = express.Router()
const sqlQuery = require('../mysql')

// const imgLists = [
//   'https://www4.bing.com//th?id=OHR.JapanHimeji_ZH-CN8344654166_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp&w=360&h=202',
//   'https://www4.bing.com//th?id=OHR.BahamasSpace_ZH-CN8053657656_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp&w=360&h=202',
//   'https://www4.bing.com//th?id=OHR.AntelopeBotswana_ZH-CN8253323519_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp&w=360&h=202'
// ]

// const createTable = async () => {
//   try {
//     const createTableSql = `
//     create table if not exists ads(
//       id int auto_increment,
//       imgUrl char(255) not null,
//       primary key (id)
//     ) engine=innodb;
//     `
//     await sqlQuery(createTable)

//     for (let i = 0; i < imgLists.length; i++) {
//       const insertSql = `insert into ads(id,imgUrl) values(null,'${imgLists[i]}')`
//       await sqlQuery(insertSql)
//     }
//   } catch (error) {}
// }
// createTable()

router.get('/advertising', async (req, res) => {
  const strSql = `select * from ads`
  try {
    const result = await sqlQuery(strSql)
    res.send({
      code: 1,
      message: '请求成功',
      result
    })
  } catch (err) {
    res.send({
      code: -1,
      message: '失败'
    })
  }
})

module.exports = router
