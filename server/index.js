const express = require('express')

const app = express()
const models = require('./models')
models(app)

const port = 3000
app.listen(port, () => {
  console.log(`listening on ${port}`)
})
